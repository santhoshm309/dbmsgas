(function(){
  var connection = require("../../index");
    var express = require('express');
    var bodyParser = require('body-parser');
    var router = express.Router();
    router.use(bodyParser.urlencoded({extended: true}));
    router.use(bodyParser.json());
    router.use(bodyParser.json({type: 'application/vnd.api+json'}));
    var RegistrationController = require('../controllers/RegistrationController');
    var AdminController = require('../controllers/AdminController');
    var LoginController = require('../controllers/LoginController');
    var CustomerController = require('../controllers/CustomerController');
    var StaffController = require('../controllers/StaffController');


    //Login and Registeration----------------------------------------------


    router.post('/insert',function(req,res) {
        RegistrationController.insert(req,res);
    });


    router.post('/addressVerify',function(req,res){
        RegistrationController.addressVerify(req,res);
    });

    router.post('/login',function(req,res){

        LoginController.login(req,res);
    });

    //Admin Controller-----------------------------------------------------


    router.post('/populateinventory' ,function(req,res){

        AdminController.populateinventory(req,res);
    });

    router.get('/count',function(req,res){
        AdminController.customerCount(req,res);
    });

    router.post('/hirestaff',function(req,res){

       AdminController.hireStaff(req,res);
    });

    router.post('/getcount',function(req,res){

        AdminController.getcount(req,res)
    });



    //Customer Controller--------------------------------------------------------



    router.post('/bookgas' , function(req,res){

        CustomerController.bookGas(req,res);
    });
    router.post('/bookcomplaint',function(req,res){
        
        CustomerController.bookComplaint(req,res);
    });



    //Staff Controller-----------------------------------------------------------------------



    router.post('/staffcomplaintview',function(req,res){

        StaffController.complaintView(req,res);
    });

    router.post('/changestatus' , function(req,res){

        StaffController.changestatus(req,res);
    });

    router.post('/staffdeliveryview' , function(req,res){

        StaffController.deliveryView(req,res);
    });

    router.post('/changedeliverystatus' , function(req,res){

        StaffController.changedeliverystatus(req,res);
    });



    module.exports = router;
})();