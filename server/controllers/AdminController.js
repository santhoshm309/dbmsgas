(function() {
    
    var db = require('../../index');
    
    module.exports.customerCount = function (req,res) {
        
        try {
            
            db.query("select customerCount()")
                .then(function(data) {
                    
                    var count = JSON.stringify(data[0]);
                    res.send(count);
                    
                },function (err) {
                    
                    console.log(err);
                    res.status(403).send();
                    
                })
            
        }catch (err) {
            res.status(500).send();
        }
        
    };

    module.exports.populateinventory = function(req,res){

            try{

                var cylindercount = req.body.cylindercount;
                if (cylindercount >= 1) {
                    db.query("select populateinventory($1)", [cylindercount])
                        .then(function onSuccess() {

                            res.send();

                        }, function onfailure(err) {

                            console.log(err);
                        })
                }else
                {
                    res.staus(403).send("Invalid cylinder count");
                }
            }   catch (err) {

                res.sendStatus(500);

            }

    };


    module.exports.hireStaff = function(req,res) {

        try {

            var name = req.body.name;
            var emailId = req.body.emailId;
            console.log(emailId);
            var accountNumber = req.body.accountNumber;
            var bank = req.body.bank;
            var salary = req.body.salary;
            var password = req.body.password;
            var gender = req.body.gender;
            var phoneNumber = req.body.phoneNumber;

            db.query("insert into staff (name,emailid,accountnumber,bank,salary,password,gender,phonenumber,role) values ($1,$2,$3,$4,$5,$6,$7,$8,$9) returning staff_id",[name,emailId,accountNumber,bank,salary,password,gender,phoneNumber,'staff'])
                .then(function onSucess(data){

                    if(data.length != 0){
                        var staff_id = data[0];
                        res.send(staff_id);
                    }else {
                        res.status(402).send("Could not insert");
                    }
            },function onFailure(err){

                    console.log(err);
                    res.sendStatus(500);
                })
        }catch (err) {

            res.sendStatus(500);

        }

    }

    module.exports.getcount= function(req,res){

        try {

            var from_date = req.body.from_date;
            var to_date = req.body.to_date;

            var total_count  ;
            var booked  ;
            var not_booked ;

            db.query("select count(*) from cylinder where date_of_entry >= $1 and date_of_entry <= $2 ",[from_date,to_date])
                .then(function onSuccees(data){


                    if(data.length == 0){

                        res.status(403).send("No inventory data found");
                    }else {
                       console.log()
                        total_count = data[0].count;
                        console.log("Total coubnt" + total_count);
                        db.query("select count(*) from cylinder where date_of_entry >= $1 and date_of_entry <= $2 and isbooked = $3", [from_date, to_date, false])
                            .then(function onSucess(data) {
                                if (data.length != 0) {

                                    not_booked = data[0].count;
                                    console.log(not_booked)
                                }
                            }, function onFailure(err) {

                            });
                        db.query("select count(*) from cylinder where date_of_entry >= $1 and date_of_entry <= $2 and isbooked = $3", [from_date, to_date, true])
                            .then(function onSuccess(data) {

                                if (data.length != 0) {
                                    //console.log(data);
                                    booked = data[0].count;
                                    console.log(booked);
                                }
                            }, function onfailure(err) {
                                res.status(500).send("Invalid dates");
                                console.log(booked);
                            });
                        console.log(booked);
                        res.json({
                            total_count: total_count,
                            notBooked: not_booked,
                            booked: booked
                        });

                    }
                },function onfailure(err){

                    console.log(err);
                })

        }catch (err) {

            res.sendStatus(500);
        }


    }
    
})();