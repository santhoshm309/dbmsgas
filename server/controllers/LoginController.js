(function(){
    
    var db= require('../../index');
    var jwt = require('jwt-simple');
    var moment = require('moment');
    var expires = moment().add(6, 'hours').valueOf();
    module.exports.login = function(req,res) {
        
        try {
            
            var emailid = req.body.emailid;
            var password = req.body.password;
            db.query("Select * from login where emailid = $1",emailid)
                .then(function(data){
                   //console.log(data);
                    /// / var userPass = JSON.stringify(data[0]);
                    if(data.length != 0) {
                        //console.log(data[0].password);
                    if(data[0].password == password){
                        var token = jwt.encode({
                            issuer : emailid,
                            expires : expires
                        },"GASBOOK");
                        if(data[0].role == 'admin')
                        res.send(token);
                        else if (data[0].role== 'staff')
                            res.status(801).send(token);
                        else if (data[0].role== 'customer')
                             res.status(802).send(token);
                    }else {
                        console.log("err 410");
                        res.status(401).send();
                        }
                    }else {

                        console.log("INvalid Username(ID)");
                        res.status(403).send("INvalid Username ID");
                    }
                },function(err){
                    console.log(err);
                    res.status(500).send();
                })
            
        }catch (err) {
            
            res.status(500).send();
        }
        
    }
    
})();