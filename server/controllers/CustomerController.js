(function(){
    
    var db = require('../../index');
    var bodyParser = require('body-parser');
    
    module.exports.bookComplaint = function(req,res) {

        try{

            var customer_id = req.body.customer_id;
            var complaint_date = new Date();
            var complaint_string = req.body.complaint_string;
            var complaint_type = req.body.complaint_type;
            var staff_id ;
             db.query("select selectstaff()")
                .then(function onSucc(data){

                 if(data){

                        staff_id = data[0].selectstaff;
                        db.query(" insert into complaint(customer_id,staff_id,complaint_type,complaint_date,complaint_string,status) values($1,$2,$3,$4,$5,$6) returning complaint_id",[customer_id,staff_id,complaint_type,complaint_date,complaint_string,false])
                            .then(function onSuccess(data){

                                if(!data){
                                    res.status(403).send("Complaint not registered");
                                }
                                else {
                                    var complaint_id = data[0].complaint_id;
                                    res.send(data);
                                }
                            });

                     }
            },function onFail(err){

                 res.sendStatus(500);
                 console.log(err);



             })


        }catch(err){


            res.sendStatus(500);
            console.log(err);

        }


    };

    module.exports.bookGas = function(req,res) {

      try{

          var customer_id = req.body.customer_id;
          var date_of_booking = new Date();
          db.query("insert into gas_booking(delivery_status,date_of_booking,isdelivered) values ($1,$2,$3) returning booking_id  ",['waiting',date_of_booking,false])
              .then(function onBookingSuccess(data){
                  if(data) {

                      var booking_id = data[0].booking_id;
                      db.query("select selectcylinder()")
                          .then(function onGasSuccess(data) {

                                  if (data.length !=0) {
                                      console.log(data);
                                      var gas_id = data[0].selectcylinder;
                                      db.query("select selectstaff()")
                                          .then(function onStaffSuccess(data) {

                                              if (data) {

                                                  var staff_id = data[0].selectstaff;
                                                  db.query("insert into rel_booking values ($1,$2,$3,$4)",[booking_id,customer_id,gas_id,staff_id])
                                                      .then(function onConfirmBooking(){

                                                          db.query("select * from bill where customer_id = $1 order by bill_id desc limit 1",[customer_id])
                                                              .then(function onBillSuccess(data){

                                                                  var billid = data[0].bill_id;
                                                                  res.send(data);
                                                              });


                                                      },function onFailureBooking(err){

                                                          console.log(err);
                                                          res.sendStatus(500);
                                                      })
                                              }

                                          },function onStaffFailure(err){

                                              console.log(err);
                                              res.sendStatus(501);
                                          })

                                  } else {

                                      res.status(404).send("No cylinder available ,book later ");

                                  }
                              },function onGasReject (err) {
                                        console.log(err);
                                        res.sendStatus(502);
                          })

                  }else{
                      res.status(403).send("Booking not confirmed")
                  }
                    //db.query("insert into rel_booking values ")

              },function onBookingFailure(err){

                            console.log(err);
                            res.sendStatus(500);
              })

      }catch(err) {

            res.sendStatus(500);

      }

    }
    
})();