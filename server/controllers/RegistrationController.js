(function(){

    var db = require("../../index");
    var bodyParser = require("body-parser");
    
    module.exports.insert = function(req,res){

        try {

            var name = req.body.name;
            var password = req.body.password;
            var address = req.body.address;
            var phoneNumber = req.body.phoneNumber;
            var gender = req.body.gender;
            var emailId = req.body.emailId;
            var reg_date = new Date();
            //var data = [name,emailId,password,address,phoneNumber,gender,false];
            db.query("INSERT INTO Customers(name,emailId,password,address,phoneNumber,gender,isVerified,reg_date) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) returning id",[name,emailId,password,address,phoneNumber,gender,false,reg_date])
               .then(function(data) {

                   var userId = JSON.stringify(data[0]);
               //    console.log(userId);
                   res.send(userId);
                    
               },function(err){
                   
                  // console.log(err);
                   if(err.code==22001){
                       res.status(403).send("Name/Adress is too long");
                   }
                   if(err.code == 23505){
                       res.status(403).send("Username /phonenumber already exists");
                   }
                   
               })
               
        }catch(err) {
            res.status(500).send();
        }
    }

    module.exports.addressVerify = function(req,res) {
        
        try{
            
            var isTrue = req.body.isTrue;
            var id = req.body.id;
            if(isTrue) {
                
                db.query("UPDATE Customers SET isVerified = $1 where id = $2",[isTrue,id])
                    .then(function(){
                        res.send();
                    },function(err){
                        console.log(err);
                    })
                
            }else {
            
                res.status(403).send("Can't verify")
                
            }
        }catch (err){
            
            res.status(500).send();
            
        }
        
        
    }
})();