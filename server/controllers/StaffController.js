(function(){

    var db = require ('../../index');
    var bodyParser = require('body-parser');

    module.exports.complaintView = function(req,res){

        try{

            var staffId = req.body.staffId;
            db.query("select * from staffcomplaintview($1)",[staffId])
                .then(function onSuccess(data){

                    if(data){

                       // var complaint = JSON.stringify(data);
                        console.log(data);
                        res.send(data);

                    }else {

                        res.status(403).send("No complaint registered !!");
                    }

                },function onFailure(err){

                  console.log(err);

                });

        }catch(err){

            res.sendStatus(500);

        }
    };

    module.exports.changestatus = function(req,res) {


        try{

            var complaint_id  = req.body.complaint_id;

            db.query("update complaint set status = $1 where complaint_id = $2", [true , complaint_id])
                .then(function onSucess(){

                    res.send();

                },function onFailure(){

                    res.status(403).send("Invalid Complaint Id");

                })

        }catch(err){

            res.sendStatus(500);
        }

    }
    module.exports.changedeliverystatus = function(req,res) {


        try{

            var booking_id  = req.body.booking_id;

            db.query("update gas_booking set isdelivered = $1 where booking_id = $2", [true , booking_id])
                .then(function onSucess(){

                    res.send();

                },function onFailure(){

                    res.status(403).send("Invalid delivery Id");

                })

        }catch(err){

            res.sendStatus(500);
        }

    }


    module.exports.deliveryView = function(req,res){

        try{

            var staffId = req.body.staffId;
            db.query("select * from staffdeliveryview($1)",[staffId])
                .then(function onSuccess(data){

                    if(data){

                        // var complaint = JSON.stringify(data);
                        //console.log(data);
                        res.send(data);

                    }else {

                        res.status(403).send("No delivery registered !!");
                    }

                },function onFailure(err){

                    console.log(err);
                    res.sendStatus(500);


                });

        }catch(err){

            res.sendStatus(500);

        }
    };

})();